FROM node:lts-alpine3.9

WORKDIR /usr/src/app

ARG EXPRESS_PORT

EXPOSE ${EXPRESS_PORT}

RUN apk add yarn

COPY package.json yarn.lock ./

COPY utils ./utils/

RUN yarn global add nodemon

RUN yarn install

COPY server.js .

COPY handlers ./handlers/
COPY public ./public/


CMD ["nodemon", "server.js"]