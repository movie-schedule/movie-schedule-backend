const fetch = require("node-fetch");
const apollo = require("./apollo");

const fetchRequest = async (variables, query) => {
  // console.log("vars in fetchreq", variables);
  // console.log("query in  fethchreq", query);
  console.log("here we go");

  try {
    const fetchResponse = await fetch(process.env.HASURA_API_ENDPOINT, {
      method: "POST",
      headers: {
        "x-hasura-admin-secret": process.env.HASURA_GRAPHQL_ADMIN_SECRET,
      },
      body: JSON.stringify({
        query,
        variables,
      }),
    });
    const data = await fetchResponse.json();

    // console.log("data in fetchreq", data);

    return data;
  } catch (error) {
    console.log(error);
  }
};

module.exports = fetchRequest;
