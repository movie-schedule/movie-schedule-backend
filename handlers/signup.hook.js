"use strict";
const gql = require("graphql-tag");
const apollo_client = require("../utils/apollo");

module.exports = async (req, res) => {
    try {


        const {
            user
        } = req.body;

        let result = await apollo_client.mutate({
            mutation: gql `
                mutation($user: [_user_insert_input!]!) {
                    insert__user(objects: $user, on_conflict: {constraint: users_pkey, update_columns: [userId, username, email, phone_number]}) {
                        id
                        roles
                    }
                }
            `,
            variables: {
                user: {
                    userId: user.id,
                    phone_number: user.phone_number,
                    email: user.email,
                    username: user.name,
                    created_by: user.id,
                }
            }
        });

        let {
            data
        } = result;

        console.log("yhew ", data);

        data.insert__user.roles.push("user");

        res.send({
            "x-hasura-user-id": data.insert__user.id,
            "x-hasura-allowed-roles": data.insert__user.roles,
            "x-hasura-default-role": "user"
        });

    } catch (error) {
        console.error(error);
        throw error;
    }
}