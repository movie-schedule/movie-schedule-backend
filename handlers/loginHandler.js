const jwt = require("jsonwebtoken");
const gql = require("graphql-tag");
const apollo = require("../utils/apollo");
const {
  OAuth2Client
} = require("google-auth-library");
// const fetchRequest = require("../utils/fetchReq");

const googleLogin = async (req, res) => {
  try {
    let token = req.body.input.token;
    const CLIENT_ID =
      "411244245336-0j370hcf06b97j5a1fkmc5pipq1mj6ju.apps.googleusercontent.com";

    const client = new OAuth2Client(CLIENT_ID);

    async function verify() {
      // try {
      const ticket = await client.verifyIdToken({
        idToken: token,
        audience: CLIENT_ID,
      });

      const payload = ticket.getPayload();
      // console.log(payload);

      const userId = payload["sub"];

      const info = {
        email: payload.email,
        username: payload.name,
        image_path: payload.picture,
        userId,
      };
      // console.log("infos hay:", info);

      const add_user_info = gql `
        mutation(
          $email: String!
          $image_path: String!
          $userId: String!
          $username: String!
        ) {
          insert_users_one(
            object: {
              email: $email
              image_path: $image_path
              userId: $userId
              username: $username
            }
          ) {
            email
            userId
            username
            role
            image_path
          }
        }
      `;
      // let {
      //   data
      // }
      const get_user_info = gql `
        query($userId: String!) {
          users(where: { userId: { _eq: $userId } }) {
            userId
            username
            email
            image_path
            role
          }
        }
      `;

      // let {
      //   data,
      //   errors
      // } = await fetchRequest({
      //   userId
      // }, `${get_user_info}`);
      let {
        data
      } = await apollo.query({
        query: get_user_info,
        variables: {
          userId
        },
      });

      // console.log("here is ", data);

      if (data.users.length === 0) {
        data = await apollo.mutate({
          mutation: add_user_info,
          variables: {
            ...info
          },
        });
      }

      return data;
      // } catch (error) {
      //   console.log("error is here", error);
      // }
    }

    const returnData = await verify();
    // console.log("returned data", returnData);
    // console.log("Response data ", returnData.users[0]);
    // console.log("Email - ", returnData.users[0].email);

    let payloadd = {
      "https://hasura.io/jwt/claims": {
        "x-hasura-allowed-roles": ["admin", "user", "cinema"],
        "x-hasura-default-role": "user",
        "x-hasura-role": returnData.users[0].role,
        "x-hasura-user-id": returnData.users[0].userId,
      },
    };
    // console.log("payload", payloadd);
    // console.log("h secret", process.env.HASURA_GRAPHQL_JWT_SECRET);

    token = jwt.sign(payloadd, '{\"type\":\"HS256\",\"key\":\"secret-keys-are-must-be-long-and-long\"}', {
      algorithm: "HS256",
      expiresIn: Date.now() + 1 * 24 * 60 * 60 * 1000,
    });

    return res.json({
      token,
      email: returnData.users[0].email,
      role: returnData.users[0].role,
      username: returnData.users[0].username,
      userId: returnData.users[0].userId,
      image_path: returnData.users[0].image_path,
    });
  } catch (error) {
    console.log(error);

  }

};

module.exports = googleLogin;