const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken")
// const fetchRequest = require("../utils/fetchReq");
const apollo_client = require("../utils/apollo");
const gql = require("graphql-tag");

const cinemaSignup = async (req, res) => {
  console.log(req.body.input);
  let info = req.body.input;
  info.password = await bcrypt.hash(info.password, 12);

  let {
    data
  } = await apollo_client.mutate({
    mutation: gql `
       mutation($name:String!,$email:String!, $address:String!,
         $password:String!)  {
           insert_cinema_one(object: {name:$name,email:$email
             address: $address, password: $password}) {
             id
             name
             email
             role
             address
             password
           }
         }
       `,
    variables: {
      ...info
    }
  })

  console.log(data.insert_cinema_one);

  payloadd = {
    "https://hasura.io/jwt/claims": {
      "x-hasura-allowed-roles": ["admin", "user", "cinema"],
      "x-hasura-default-role": "cinema",
      "x-hasura-role": data.insert_cinema_one.role,
      "x-hasura-user-id": data.insert_cinema_one.id,
    },
  };

  const token = jwt.sign(payloadd, process.env.HASURA_GRAPHQL_JWT_SECRETS, {
    algorithm: "HS512",
    expiresIn: Date.now() + process.env.JWT_EXPIRE_TIME * 24 * 60 * 60 * 1000,
  })

  return res.json({
    token,
    id: data.insert_cinema_one.id,
    name: data.insert_cinema_one.name,
    email: data.insert_cinema_one.email,
    role: data.insert_cinema_one.role,
    address: data.insert_cinema_one.address,
    password: data.insert_cinema_one.password,
  });
};

module.exports = cinemaSignup;