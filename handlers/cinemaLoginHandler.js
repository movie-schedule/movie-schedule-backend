const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
// const fetchRequest = require("../utils/fetchReq");
const apollo_client = require("../utils/apollo");
const gql = require("graphql-tag");

const cinemaLogin = async (req, res) => {
    correctPassword = async function (candidatePassword, userPassword) {
        return await bcrypt.compare(candidatePassword, userPassword);
    };
    // console.log("incoming data", req.body.input);
    const email = req.body.input.email;
    // console.log("email=", email);

    let q = gql `
    query($email: String!) {
      cinema(where: { email: { _eq: $email } }) {
        id
        name
        email
        role
        address
        password
      }
    }
  `;
    // let data = await apollo_client.query({
    //     query: gql `
    //         query($email: String!) {
    //           cinema(where: {email: {_eq: $email}}) {
    //             id
    //             name
    //             email
    //             role
    //             address
    //             password
    //           }
    //         }
    //         `,
    //     variables: {
    //         email
    //     }
    // })

    let {
        data
    } = await apollo_client.query({
        query: q,
        variables: {
            email,
        },
    });

    // console.log("no truth value", data.cinema);
    let val = await correctPassword(
        req.body.input.password,
        data.cinema[0].password
    );
    console.log("****** truth value *****", val);

    if (
        !data ||
        !(await correctPassword(req.body.input.password, data.cinema[0].password))
    ) {
        return Error("Invalid email or password");
    }

    payloadd = {
        "https://hasura.io/jwt/claims": {
            "x-hasura-allowed-roles": ["admin", "user", "cinema"],
            "x-hasura-default-role": "cinema",
            "x-hasura-role": data.cinema[0].role,
            "x-hasura-user-id": data.cinema[0].id.toString(),
        },
        "metadata": {
            "x-hasura-allowed-roles": ["admin", "user", "cinema"],
            "x-hasura-default-role": "cinema",
            "x-hasura-role": data.cinema[0].role,
            "x-hasura-user-id": data.cinema[0].id.toString(),
        },
    };

    // console.log("jwt", process.env.HASURA_GRAPHQL_JWT_SECRETS.replace(/\\n/gm, '\n'));

    const token = jwt.sign(payloadd, process.env.HASURA_GRAPHQL_JWT_SECRETS, {
        algorithm: "RS256",
        expiresIn: Date.now() + process.env.JWT_EXPIRE_TIME * 24 * 60 * 60 * 1000,
    });

    console.log(token);

    return res.json({
        token,
        id: data.cinema[0].id,
        name: data.cinema[0].name,
        email: data.cinema[0].email,
        role: data.cinema[0].role,
        address: data.cinema[0].address,
    });
};

module.exports = cinemaLogin;