const base64Img = require('base64-img')

const imageUpload = async (req, res) => {

    const { image } = req.body.input

    try {

      let image_path = ''
      
      // Insert into the servers public folder
      base64Img.img(image, './public', Date.now(), function(err, filepath) {
        const pathArr = filepath.split('/')
        const fileName = pathArr[pathArr.length - 1]
        image_path = `http://127.0.0.1:${process.env.PORT}/${fileName}`

        res.status(200).json({
          imagePath: image_path
        })
      })

    } 
    catch (error) {
        return res.status(400).json({
            message: error.message
        })
    }
}

module.exports = imageUpload
