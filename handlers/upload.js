// const fetchRequest = require('../utils/fetchReq')

// const base64Img = require("base64-img");

// const upload = async (req, res) => {
//     const {
//         image
//     } = req.body.input;

//     try {
//         let movieId = req.body.input.movieId;

//         base64Img.img(image, "./public", Date.now(), async function (err, filepath) {
//             const add_image_data = `
//             mutation($path:String!, $movieId:Int!)  {
//                 insert_images_one(object: {path: $path, 
//                   movieId: $movieId,}) {
//                   id
//                   path
//                   movieId
//                 }
//               }
//             `;

//             const pathArr = filepath.split("/");
//             const path = pathArr[pathArr.length - 1];


//             let info = {
//                 path,
//                 movieId
//             }

//             let {
//                 data,
//                 error
//             } = await fetchRequest({
//                 ...info
//             }, `${add_image_data}`);

//             res.status(200).json({
//                 id: data.insert_images_one.id,
//                 path,
//                 movieId
//             });
//         });
//     } catch (error) {
//         return res.status(400).json({
//             message: error.message,
//         });
//     }
// };

// module.exports = upload;
const cloudinary = require("cloudinary").v2;

const upload = async (req, res) => {

    try {

        const {
            image,
            folder
        } = req.body.input;

        // console.log(req.body.input);


        let secure_urls = "";

        let urls = "";

        // for (let file in files) {

        //     file = files[file];

        let data = await cloudinary.uploader.upload(image, {
            unique_filename: true,
            discard_original_filename: true,
            folder: folder,
            timeout: 120000
        });

        secure_urls = data.secure_url

        urls = data.url

        // }

        res.send({
            secure_urls,
            urls
        });


    } catch (error) {

        console.error(error);

        res.status(500).send({
            message: "Error Uploading Files",
            code: "error_uploading_file"
        });

    }

};

module.exports = upload;