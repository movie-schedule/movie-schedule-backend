"use strict";
const gql = require("graphql-tag");
const apollo_client = require("../utils/apollo");

module.exports = async (req, res) => {
    try {


        const {
            user
        } = req.body;

        let result = await apollo_client.mutate({
            mutation: gql `
                mutation($user: [_user_insert_input!]!) {
                    insert__user(objects: $user, on_conflict: {constraint: users_pkey, update_columns: [userId, email, phone_number]}) {
                        returning{
                         userId
                         role
                        }
                    }
                }
            `,
            variables: {
                user: {
                    userId: user.id,
                    phone_number: user.phone_number,
                    email: user.email,
                    username: user.name,
                    created_by: user.id
                }
            }
        });

        let data = result.data.insert__user.returning[0];

        // data.role = "user";

        res.send({
            "x-hasura-user-id": data.userId,
            "x-hasura-allowed-roles": ["admin", "user", "cinema"],
            "x-hasura-default-role": "user"
        });

    } catch (error) {
        console.error(error);
        throw error;
    }
}