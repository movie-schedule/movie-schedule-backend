const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken")

const fetchRequest = require("../utils/fetchReq");

const passwordUpdate = async (req, res) => {
    console.log(req.body.input);

    correctPassword = async function (
        candidatePassword,
        userPassword
    ) {
        return await bcrypt.compare(candidatePassword, userPassword);
    };
    let id = req.body.input.id;
    let password = await bcrypt.hash(req.body.input.newPassword, 12);

    const get_cinema_info = `
    query($id: Int!) {
      cinema(where: {id: {_eq: $id}}) {
        id
        email
        role
        address
        password
      }
    }
    `;

    let {
        data,
        errors
    } = await fetchRequest({
            id
        },
        `${get_cinema_info}`
    );

    console.log("here data", data);


    const get_updated_info = `
    mutation($password:String!,$id:Int!){ update_cinema_by_pk(pk_columns: {id:$id},_set: {password:$password,id:$id}){
        id
        name
        address
        role
      }}
   
    `;
    let variables = {
        password,
        id
    }
    if (await correctPassword(req.body.input.currentPassword, data.cinema[0].password)) {
        let {
            data,
            errors
        } = await fetchRequest(variables,
            `${get_updated_info}`
        );

        // console.log("updated data", data);

        payloadd = {
            "https://hasura.io/jwt/claims": {
                "x-hasura-allowed-roles": ["admin", "user", "cinema"],
                "x-hasura-default-role": "cinema",
                "x-hasura-role": data.update_cinema_by_pk.role,
                "x-hasura-user-id": data.update_cinema_by_pk.id.toString(),
            },
        };
        const ids = data.update_cinema_by_pk.id;
        const token = jwt.sign(payloadd, process.env.HASURA_GRAPHQL_JWT_SECRET, {
            algorithm: "HS256",
            expiresIn: Date.now() + process.env.JWT_EXPIRE_TIME * 24 * 60 * 60 * 1000,
        })

        return res.json({
            id: ids,
            token
        });

    }


}

module.exports = passwordUpdate;