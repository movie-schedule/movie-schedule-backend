const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const PORT = process.env.EXPRESS_PORT;
// Import controllers for functionalities
// const imageHandler = require('./controllers/users/imageHandler')
const loginHandler = require('./handlers/loginHandler')
const cinemaSignup = require('./handlers/cinemaSignupHandler')
const cinemaLogin = require('./handlers/cinemaLoginHandler')
const upload = require('./handlers/upload')
const reminder = require('./handlers/reminder')
const passwordUpdate = require('./handlers/passwordUpdate')

require("dotenv").config();
const app = express();

app.use(cors())
app.use(bodyParser.json({
    limit: '50mb'
}))
app.use(express.static('./public'));

// // Routes
app.post("/loginhook", require("./handlers/login.hook"));
app.post("/signuphook", require("./handlers/signup.hook"))
app.post('/upload', upload)
app.post('/loginGoogle', loginHandler)
app.post('/cinemaSignup', cinemaSignup)
app.post('/cinemaLogin', cinemaLogin)
app.post('/reminder', reminder)
app.post('/passwordUpdate', passwordUpdate)

app.listen(PORT, () => console.log(`server running on port ${PORT}`));